import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { AdminDashboardComponent } from './Admin/admin-dashboard/admin-dashboard.component';
import { StudentDashboardComponent } from './Student/student-dashboard/student-dashboard.component';
import { TeachersComponent } from './Teacher/teacher-dashboard/teachers.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './Student/register/register.component';
import { StudentsAddComponent } from './Student/add/students.add.component';
import { UpdateComponent } from './Student/update/update.component';
import { StudentsComponent } from './Student/list/students.component';
import { MyNavComponent } from './my-nav/my-nav.component';
import { ActivityTableComponent } from './Student/activity-table/activity-table.component';
import { ViewComponent } from './Teacher/view/view.component';
import { MixdataTableComponent } from './Teacher/mixdata-table/mixdata-table.component';
import { AddComponent } from './Admin/add/add.component';
import { CheckComponent } from './Admin/check/check.component';

// const appRoutes: Routes = [

//        {path:'', redirectTo: 'my-nav', pathMatch:'full'},
// // { path: 'my-nav', component: HomeComponent},
//       { path: 'home', component: HomeComponent},
//       { path: 'admin-dashboard', component: AdminDashboardComponent },
//       { path: 'student-dashboard', component: StudentDashboardComponent },
//       { path: 'teacher-dashboard', component:  TeachersComponent },
// //      { path: 'student-dashboard/register', component:  RegisterComponent },
//  //     { path: 'student-dashboard/list', component:  StudentsComponent },
// //      { path: 'student-dashboard/update', component: UpdateComponent },
// //      { path: 'student-dashboard/add', component:  StudentsAddComponent }  
//        { path: 'teacher-dashboard/view', component:  ViewComponent },
//       // { path: 'teacher-dashboard/view/update', component:  UpdateTeacherComponent },
//        //{ path: 'teacher-dashboard/decision', component:  MixdataTableComponent },
//        //{ path: 'teacher-dashboard/update', component:  UpdateComponent },
//        { path: 'admin-dashboard/add', component:  AddComponent },
//        { path: 'admin-dashboard/check', component:  CheckComponent },
// ];
const appRoutes: Routes = [
    {
      path: '',
      redirectTo: '/home',
      pathMatch: 'full'
    }
    
  
  ];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
