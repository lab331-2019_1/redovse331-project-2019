import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  creat(): void {
    
    Swal.fire({
      title: 'Do you want creat a new activity ?',
      //text: "You won't be able to revert this!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#c2185b',
      cancelButtonColor: 'black',
      confirmButtonText: 'Yes, Creat'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Created successfully',
           '',
          'success',
        )
        
      }
    })
  }
}
