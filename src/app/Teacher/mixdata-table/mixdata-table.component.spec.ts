import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MixdataTableComponent } from './mixdata-table.component';

describe('MixdataTableComponent', () => {
  let component: MixdataTableComponent;
  let fixture: ComponentFixture<MixdataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MixdataTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MixdataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
