export default class Activity {
  id:number;
  activityname:string;
  activitydescription:string;
  periodstart:string;
  periodend:string;  
  Date:string;
  professor:string;
  image:string;
  description:string;

}
