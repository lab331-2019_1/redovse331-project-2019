import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Mixdata from '../entity/mixdata';
import { map } from 'rxjs/operators';
import { MixdataService } from './mixdata.service';

@Injectable({
  providedIn: 'root'
})
export class MixdataFileImplService extends MixdataService {

  constructor(private http: HttpClient) {
    super();

  }
  getMixdatas(): Observable<Mixdata[]> {
    return this.http.get<Mixdata[]>('assets/mixdata.json');
  }
  getMixdata(id :number): Observable<Mixdata> {
    return this.http.get<Mixdata[]>('assets/mixdata.json')
    .pipe(map(mixdatas => {
      const output: Mixdata = 
      ( mixdatas as Mixdata[]).find
        (mixdatas => mixdatas.id === +id);
        return output;
    }));
  }

}