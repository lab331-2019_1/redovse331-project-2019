import { Observable } from 'rxjs';
import Mixdata from '../entity/mixdata';

export abstract class MixdataService{
     abstract getMixdatas(): Observable<Mixdata[]>;
     abstract getMixdata(id : number): Observable<Mixdata>;
     
}
