import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Activity from 'src/app/entity/activity';
import { ActivityTableDataSource } from './activity-table-datasource';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-activity-table',
  templateUrl: './activity-table.component.html',
  styleUrls: ['./activity-table.component.css']
})
export class ActivityTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'activityname', 'activitydescription', 'period','Date','location','host','status','RemoveBtn'];
  constructor(private activityService: ActivityService) { }
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  ngOnInit() {
    this.dataSource = new ActivityTableDataSource();
    this.activityService.getActivitys()
    .subscribe(activities => {
      this.dataSource = new ActivityTableDataSource();
      this.dataSource.data = activities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.activities = this.activities;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
    }
    );
  }

  ngAfterViewInit() {}
    
    
    
    applyFilter(filterValue: string) {
      this.filter$.next(filterValue.trim().toLowerCase());
    }
  
    confirm() {
      if (confirm("Are you sure to enroll this activity ?")) {
        window.location.pathname = '/enrolledPage'
      }
    }

    openDialog(): void {
   
      Swal.fire({
        title: 'Do you want to remove ?',
        //text: "You won't be able to revert this!",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#c2185b',
        cancelButtonColor: 'black',
        confirmButtonText: 'Yes，Remove',
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Removed successfully',
             '',
            'success',
          )
          
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          Swal.fire(
            'Cancel',
            '',
            'error'
          )
        }
      })
      
      
    }



  }

