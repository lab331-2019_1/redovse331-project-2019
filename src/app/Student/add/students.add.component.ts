import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import Student from '../../entity/student';
import { FormBuilder } from '@angular/forms';
import { MatPaginator, MatSort, MatTable } from '@angular/material';
import { ActivityTableDataSource } from '../activity-table/activity-table-datasource';
import Activity from 'src/app/entity/activity';
import Swal from 'sweetalert2';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity.service';


@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent implements AfterViewInit, OnInit{
  students: Student[];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;

  displayedColumns = ['id', 'activityname', 'activitydescription', 'period','Date','location','host','AddBtn'];
  constructor(private activityService: ActivityService) { }
  activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  ngOnInit() {
    this.dataSource = new ActivityTableDataSource();
    this.activityService.getActivitys()
    .subscribe(activities => {
      this.dataSource = new ActivityTableDataSource();
      this.dataSource.data = activities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.activities = this.activities;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      
    }
    );
  }

  ngAfterViewInit() {}
    
    
    
    applyFilter(filterValue: string) {
      this.filter$.next(filterValue.trim().toLowerCase());
    }
  
    confirm() {
      if (confirm("Are you sure to enroll this activity ?")) {
        window.location.pathname = '/enrolledPage'
      }
    }

    openDialog(): void {
   
      Swal.fire({
        title: 'Do you want to add the activity ?',
        //text: "You won't be able to revert this!",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#c2185b',
        cancelButtonColor: 'black',
        confirmButtonText: 'Add',
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Add successful',
             '',
            'success',
          )
          
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          Swal.fire(
            'Cancell add',
            '',
            'error'
          )
        }
      })
      
      
    }



}
