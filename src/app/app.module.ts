import * as platformBrowser from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentService } from './service/student-service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentsFileImplService } from './service/students-file-impl.service';
import { StudentsComponent } from './Student/list/students.component';
import { StudentsAddComponent } from './Student/add/students.add.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import { AppRoutingModule } from './app-routing.module';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { HomeComponent } from './home/home.component';
import { AdminDashboardComponent } from './Admin/admin-dashboard/admin-dashboard.component';
import { TeachersComponent } from './Teacher/teacher-dashboard/teachers.component';
import { StudentRoutingModule } from './Student/student-routing';
import { StudentDashboardComponent } from './Student/student-dashboard/student-dashboard.component';
import { RegisterComponent } from './Student/register/register.component';
import { UpdateComponent } from './Student/update/update.component';



import { ActivityFileImplService } from './service/activity-file-impl.service';
import { ActivityTableComponent } from './Student/activity-table/activity-table.component';
import { ActivityService } from './service/activity.service';
import { ViewComponent } from './Teacher/view/view.component';


import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { DecisionComponent } from './Teacher/decision/decision.component';
import { MixdataTableComponent } from './Teacher/mixdata-table/mixdata-table.component';
import { MixdataService } from './service/mixdata.service';
import { MixdataFileImplService } from './service/mixdata-file-impl.service';
import { AddComponent } from './Admin/add/add.component';
import { CheckComponent } from './Admin/check/check.component';
import { UpdateListComponent } from './Teacher/update-list/update-list.component';

//import { MatFileUploadModule } from 'angular-material-fileupload';
 

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsAddComponent,
    MyNavComponent,
    FileNotFoundComponent,
    TeachersComponent,
    AdminDashboardComponent,
    HomeComponent,
    AdminDashboardComponent,
    StudentDashboardComponent,
    RegisterComponent,
    UpdateComponent,
    ActivityTableComponent,
    ViewComponent,
    DecisionComponent,
    MixdataTableComponent,
    AddComponent,
    CheckComponent,
    UpdateListComponent

  ],
  imports: [
    platformBrowser.BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSortModule,
    AppRoutingModule,
    StudentRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    
    //MatFileUploadModule


    
  ],
  providers: [
    { provide: ActivityService, useExisting: ActivityFileImplService },
    { provide: StudentService, useExisting: StudentsFileImplService },
    { provide: MixdataService, useClass: MixdataFileImplService }
  ],
 
  bootstrap: [AppComponent]
})
export class AppModule { }
